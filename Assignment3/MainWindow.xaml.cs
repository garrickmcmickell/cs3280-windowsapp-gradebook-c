﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Assignment3
{   
    /// <summary>
    /// The student class creates a student object and contains get/set methods for relevant 
    /// information.
    /// </summary>
    class Student
    {
        #region Class level variables
        /// <summary>
        /// This saves the student name.
        /// </summary>
        string studentName;
        /// <summary>
        /// This saves the student number.
        /// </summary>
        int studentNo;
        /// <summary>
        /// An integer array to store the scores for each assignment.
        /// </summary>
        int[] studentScores;
        #endregion

        #region Methods
        /// <summary>
        /// This is the constructor for the student class. It receives a name, number, and size to set
        /// the studentScores array.
        /// </summary>
        /// <param name="name">The name of the student.</param>
        /// <param name="no">The student's number.</param>
        /// <param name="size">The number of assignments.</param>
        public Student(string name, int no, int size)
        {
            studentName = name;
            studentNo = no;
            studentScores = new int[size];
        }

        /// <summary>
        /// Returns the student's name.
        /// </summary>
        /// <returns></returns>
        public string getName()
        {
            return studentName;
        }

        /// <summary>
        /// Sets the student's name.
        /// </summary>
        /// <param name="name">The new name of the student.</param>
        public void setName(string name)
        {
            studentName = name;
        }

        /// <summary>
        /// Returns the student's number.
        /// </summary>
        /// <returns></returns>
        public int getStudentNo()
        {
            return studentNo;
        }

        /// <summary>
        /// Returns the number of assignments.
        /// </summary>
        /// <returns></returns>
        public int getNoAssignments()
        {
            return studentScores.Length;
        }

        /// <summary>
        /// Sets the assignment score at the given value.
        /// </summary>
        /// <param name="index">0 based index for which assignment the score is for.</param>
        /// <param name="score">The score on the assignment.</param>
        public  void setAssignmentScore(int index, int score)
        {
            studentScores[index] = score;
        }

        /// <summary>
        /// Returns the assignment score at the given index.
        /// </summary>
        /// <param name="index">0 based index for which assignment score to return.</param>
        /// <returns></returns>
        public int getAssignmentScore(int index)
        {
            return studentScores[index];
        }

        /// <summary>
        /// Returns the average score for all assignments.
        /// </summary>
        /// <returns></returns>
        public double getAverage()
        {
            double average = 0;

            for (int i = 0; i < studentScores.Length; i++)
                average += studentScores[i];

            average /= getNoAssignments();

            return Math.Round(average, 2); 
        }

        /// <summary>
        /// Returns the letter grade of the student based off of the average score for assignments.
        /// </summary>
        /// <returns></returns>
        public string getGrade()
        {
            double average = getAverage();

            if(average >= 93) { return "A"; }
            if (average >= 90) { return "A-"; }
            if (average >= 87) { return "B+"; }
            if (average >= 83) { return "B"; }
            if (average >= 80) { return "B-"; }
            if (average >= 77) { return "C+"; }
            if (average >= 73) { return "C"; }
            if (average >= 70) { return "C-"; }
            if (average >= 67) { return "D+"; }
            if (average >= 63) { return "D"; }
            if (average >= 60) { return "D-"; }
            if (average < 60) { return "E"; }

            return "";
        }
        #endregion
    }

    public partial class MainWindow : Window
    {
        #region Class level variables
        /// <summary>
        /// An array of student objects. Used to store the created student objects and their information.
        /// </summary>
        Student[] arrStudents;
        /// <summary>
        /// A student object to keep track of the selected student to modify and retrieve information.
        /// </summary>
        Student stuCurrStudent;
        /// <summary>
        /// A boolean variable to keep track of whether arrStudents has objects currently within it.
        /// </summary>
        bool boolStudentsCreated = false;

        /// <summary>
        /// Delegate used to display a message on the UI thread
        /// </summary>
        private delegate void DisplayMessageDelegate();

        string fileName;

        public object DisplayMessage { get; private set; }
        #endregion

        #region Methods
        /// <summary>
        /// Initializes main window.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Populates arrStudents with number of students based off of what was entered in tbNumStudents.
        /// Initializes and creates each student based off of the value entered in tbNumAssignments.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btSubmitCounts_Click(object sender, RoutedEventArgs e)
        {
            int iNumStudents;
            int iNumAssignments;
            if(Int32.TryParse(tbNumStudents.Text, out iNumStudents) == true &&
                Int32.TryParse(tbNumAssignments.Text, out iNumAssignments) == true &&
                iNumStudents < 10 && iNumStudents > 0 && iNumAssignments < 100 && 
                iNumAssignments > 0 && boolStudentsCreated == false)
            { 
                arrStudents = new Student[iNumStudents];

                for (int i = 0; i < arrStudents.Length; i++)
                {
                    arrStudents[i] = new Student("Student" + (i + 1), i, iNumAssignments);
                }

                stuCurrStudent = arrStudents[0];
                tbStudentName.Text = stuCurrStudent.getName();
                lbStudentName.Content = "Student #" + (stuCurrStudent.getStudentNo() + 1) + ":";

                lbAssignmentNo.Content = "Enter Assignment Number (1-" + arrStudents[0].getNoAssignments() + "):";

                boolStudentsCreated = true;
            }
        }

        /// <summary>
        /// Sets stuCurrStudent to the first student in arrStudents.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btFirstStudent_Click(object sender, RoutedEventArgs e)
        {
            if (boolStudentsCreated == true)
            {
                stuCurrStudent = arrStudents[0];
                tbStudentName.Text = stuCurrStudent.getName();
                lbStudentName.Content = "Student #" + (stuCurrStudent.getStudentNo() + 1) + ":";
            }
        }

        /// <summary>
        /// Sets stuCurrStudent to the last student in arrStudents.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btLastStudent_Click(object sender, RoutedEventArgs e)
        {
            if (boolStudentsCreated == true)
            {
                stuCurrStudent = arrStudents[arrStudents.Length - 1];
                tbStudentName.Text = stuCurrStudent.getName();
                lbStudentName.Content = "Student #" + (stuCurrStudent.getStudentNo() + 1) + ":";
            }
        }

        /// <summary>
        /// Sets stuCurrStudent to the previous student in arrStudents.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btPreviousStudent_Click(object sender, RoutedEventArgs e)
        {
            if (boolStudentsCreated == true)
            {
                if (stuCurrStudent != arrStudents[0])
                {
                    stuCurrStudent = arrStudents[stuCurrStudent.getStudentNo() - 1];
                    tbStudentName.Text = stuCurrStudent.getName();
                    lbStudentName.Content = "Student #" + (stuCurrStudent.getStudentNo() + 1) + ":";
                }
            }
        }

        /// <summary>
        /// Sets stuCurrStudent to the next student in arrStudents.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btNextStudent_Click(object sender, RoutedEventArgs e)
        {
            if (boolStudentsCreated == true)
            {
                if (stuCurrStudent != arrStudents[arrStudents.Length - 1])
                {
                    stuCurrStudent = arrStudents[stuCurrStudent.getStudentNo() + 1];
                    tbStudentName.Text = stuCurrStudent.getName();
                    lbStudentName.Content = "Student #" + (stuCurrStudent.getStudentNo() + 1) + ":";
                }
            }
        }

        /// <summary>
        /// Sets stuCurrStudent name to the value in tbStudentName
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btSaveName_Click(object sender, RoutedEventArgs e)
        {
            if (boolStudentsCreated == true)
            {
                stuCurrStudent.setName(tbStudentName.Text);
            }
        }

        /// <summary>
        /// Saves the value in tbAssigmentScore to the index entered in tbAssignmentNo of the student's score array.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btSaveScore_Click(object sender, RoutedEventArgs e)
        {
            if (boolStudentsCreated == true)
            {
                int iAssignmentNo;
                int iAssignmentScore;
                if (Int32.TryParse(tbAssignmentNo.Text, out iAssignmentNo) == true &&
                    Int32.TryParse(tbAssignmentScore.Text, out iAssignmentScore) == true &&
                    iAssignmentNo <= stuCurrStudent.getNoAssignments() && iAssignmentNo > 0 &&
                    iAssignmentScore <= 100 && iAssignmentScore > 0)
                {
                    stuCurrStudent.setAssignmentScore(iAssignmentNo - 1, iAssignmentScore);
                }
            }
        }

        /// <summary>
        /// Accesses the scores array for each student object in arrStudent and displays them into tBlScoreDisplay.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btDisplayScores_Click(object sender, RoutedEventArgs e)
        {
            if (boolStudentsCreated == true)
            {
                tBlScoreDisplay.Text = "STUDENT                 ";

                for (int i = 0; i < stuCurrStudent.getNoAssignments(); i++)
                {
                    tBlScoreDisplay.Text += "#" + (i + 1) + "        ";
                }

                tBlScoreDisplay.Text += "AVG       GRADE\n";
                for (int i = 0; i < arrStudents.Length; i++)
                {
                    tBlScoreDisplay.Text += String.Format("{0,-26}", arrStudents[i].getName());

                    for (int j = 0; j < arrStudents[i].getNoAssignments(); j++)
                    {
                        tBlScoreDisplay.Text += String.Format("{0,-11}", arrStudents[i].getAssignmentScore(j));
                    }

                    tBlScoreDisplay.Text += String.Format("{0,-11}", arrStudents[i].getAverage()) + String.Format("{0,-11}", arrStudents[i].getGrade()) + "\n";
                }
            }
        }

        /// <summary>
        /// Sets arrStudent to null so that it can be reused and sets boolStudentsCreated to false so that new students can be created. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btResetScores_Click(object sender, RoutedEventArgs e)
        {
            if (boolStudentsCreated == true)
            {
                arrStudents = null;
                boolStudentsCreated = false;
                //for (int i = 0; i < arrStudents.Length; i++)
                //    for (int j = 0; j < arrStudents[i].getNoAssignments(); j++)
                //        arrStudents[i].setAssignmentScore(j, 0);
            }
        }
        #endregion

        /// <summary>
        /// Saves the file name, creates a thread, and runs the thread method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btpRINT_Click(object sender, RoutedEventArgs e)
        {
            fileName = tbFileName.Text;

            tbFileName.Text = "Writing to File";

            Thread MyThread = new Thread(new ThreadStart(MyThreadMethod));
            
            MyThread.Start();
        }

        /// <summary>
        /// Puts the thread to sleep and starts the writeToFile method.
        /// </summary>
        private void MyThreadMethod()
        {
            Thread.Sleep(3000);

            this.Dispatcher.BeginInvoke(new DisplayMessageDelegate(writeToFile));
        }

        /// <summary>
        /// Writes the contents of the text block to a file.
        /// </summary>
        public void writeToFile()
        {
            btpRINT.IsEnabled = false;

            string sFile = @"C:\" + fileName + ".txt";

            //Make sure the file exists
            if (File.Exists(sFile))
            {
                MessageBox.Show("This file already exist");
            }
            else
            {
                //Create the stream writer

                
                using (StreamWriter MyWriter = new StreamWriter(sFile))
                {

                    //Write the data to the file
                    MyWriter.Write(tBlScoreDisplay.Text);
                }
            }

            btpRINT.IsEnabled = true;

            tbFileName.Text = "Finished writing to File";
        }

        /// <summary>
        /// Determines if the file can be saved or not.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbFileName_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = (TextBox)sender;

            if (tb.Text == "")
            {
                btpRINT.IsEnabled = false;
            }
            else
            {
                btpRINT.IsEnabled = true;
            }
        }
    }
}
